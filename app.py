from flask import Flask, render_template, request, redirect, url_for, make_response,g,abort,session
import os
import functools, os
import click
from http import HTTPStatus
import fonction.dbfonction as db
import fonction.db_insert as dbi
secret_key = os.environ.get("SECRET_KEY")
import os


def get_db_conn():

    if "conn" not in g:
        g.conn = db.connection()
    return g.conn


def close_db_conn(e=None):

    conn = g.pop("conn", None)
    if conn:
        db.DeconnexionBD(conn)

def load_logged_in_user():
    user_id = session.get("user.id")

    if user_id is None:
        g.user = None
    else:
        conn = get_db_conn()
        g.user = db.get_utilisateur(conn, user_id) 

def load_layout():
    user_role = session.get("user.role")
    if g.user is not None and user_role == 'master':
        session.layout = "layout_admin.html"
    

app = Flask(__name__)
app.secret_key = secret_key
app.before_request(load_logged_in_user)
app.before_request(load_layout)

app.teardown_appcontext(close_db_conn)

############################# COMMANDE FLASK ############################################################################
#########################################################################################################################
################################################################################################################


@app.cli.command("init-db")
def init_db_command():
    conn = get_db_conn()
    db.create_db_schema(conn)
    #DBInit.InitialisationDB(conn)
    click.echo("Initialized the database.")




@app.cli.command("drop-db")
def drop_db_command():
    conn = get_db_conn()
    db.drop_db_schema(conn)
    #DBInit.DropTableDB(conn)
    click.echo("Dropped the database.")


@app.cli.command("create-user")
@click.argument("username")
@click.argument("password")
@click.argument("role")
@click.argument("group")
def create_user_command(username, password, role,group):
    conn = get_db_conn()
    db.create_user(conn, username, password, role,group)
    click.echo(f"User {username} created with role {role} and group {group}")


@app.cli.command("generate-secret-key")
@click.argument("nbytes", default=32)
def generate_secret_command(nbytes):
    import secrets

    new_secret_key = secrets.token_hex(nbytes)
    click.echo(f"New secret key: {new_secret_key}")


@app.cli.command("decode-session-cookie")
@click.argument("session_cookie")
def decode_session_cookie_command(session_cookie):
    from flask.sessions import SecureCookieSessionInterface

    ssci = SecureCookieSessionInterface()
    serializer = ssci.get_signing_serializer(app)
    session_cookie_values = serializer.loads(session_cookie)

    click.echo(f"Session cookie values: {session_cookie_values}")


########################## decorateur ########################################################
################################################################################################################


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(render_template("pages/login.html"))
            #abort(HTTPStatus.UNAUTHORIZED)
        return view(**kwargs)

    return wrapped_view

def role_required(role):
    def role_required_decorator(view):
        @functools.wraps(view)
        def wrapped_view(**kwargs):
            if g.user is not None and g.user["role"] == role:
                return view(**kwargs)
            abort(HTTPStatus.FORBIDDEN)

        return wrapped_view

    return role_required_decorator

    
##############################    APP ROUTE     ######################################

@app.route("/", methods=["GET"])
def get_connexion():
    return render_template("pages/login.html")

@login_required
@role_required("master")
@app.route("/admin", methods=["GET"])
def get_inscription():

    return render_template("pages/inscription.html")


@login_required
@role_required("master")
@app.route("/inscription", methods=["POST"])
def post_inscription():
    conn= get_db_conn() 
    pseudo = request.form["uname"]
    password = request.form["psw"]
    password_repeat = request.form.get("psw-repeat")
    group = session["user.group_name"]
    if password_repeat == password:
        
        try:
            db.create_user(conn=conn, username=pseudo, password=password, role="student", group=group)
        except ValueError:
            return render_template('pages/inscription.html', errorMessage=f"Le pseudo : {pseudo} existe , rentrez un autre pseudo!")


        return render_template('pages/inscription.html', errorMessage=f"l'utilisateur {pseudo} est enregistre")

    else:
        return render_template('pages/inscription.html', errorMessage="Vos 2 passwords sont différents !")

@app.route('/board_student', methods=['GET'])
@login_required
def get_home_eleve():
    id_user = request.args.get("user")
    conn = get_db_conn()
    user= db.selection_user(conn,id_user)
    role=user[2]
    
    liste_measure = db.select_last_liste_measure(conn, role, id_user,session['user.group_name'])
    if liste_measure == None :
        return render_template('pages/homepage_student_none.html',username = user[1])
    path_imgs = dbi.histograme_equivalence(liste_measure[1],id_user)
    
    #dbi.histograme_equivalence(liste_measure[1],id_user)
    return render_template('pages/homepage_student.html',username = user[1], liste_measure = liste_measure,plot = path_imgs)

@app.route('/home', methods=['GET'])
@login_required
def get_home():
    conn = get_db_conn()
    liste_measure = db.select_last_liste_measure(conn, session["user.role"], session['user.id'],session['user.group_name'])
    
    if liste_measure == None :
        return render_template('pages/homepage_student_none.html', liste_id_user = liste_measure)
    if session['user.role']=='master':

        return render_template('pages/homepage.html', liste_id_user = liste_measure)

    elif session['user.role']=='student':
        
        # path_img = "static/img/" + str(session['user.id']) +".png"
        path_imgs = dbi.histograme_equivalence(liste_measure[1],session['user.id'])
        return render_template('pages/homepage_student.html', liste_measure = liste_measure, username = session['user.pseudo'],plot= path_imgs)

    else:
        return abort('404')
    #return render_template('pages/homepage.html')

@app.route('/histogram', methods=['GET'])
@login_required
def get_graph():
    conn = get_db_conn()
    liste_measure = db.select_last_liste_measure(conn, session["user.role"], session['user.id'],session['user.group_name'])
    plt = dbi.histograme_equivalence(liste_measure[1],session['user.id'])

    return render_template('pages/histogram.html', plt = plt)

@app.route("/login", methods=["POST"])
def post_login():
    session.clear()
    pseudo = request.form.get("uname")
    password = request.form.get("psw")
    conn= get_db_conn()
    try:
        
        users = db.authentification_utilisateur(conn=conn, username=pseudo, password=password)
        print(users)
        
        name_user_group = users[1][0]
        user = users[0]
        user_role= user[3]
        print(user_role)
        user_id = user[0]
        session['user.pseudo']=user[1]

    except ValueError:
        return render_template('pages/login.html', errorMessage="Vos identifiant sont incorect !")
    
    print(user)
    
    

    response = None
    if user_role == db.UserRoleEnum.student:
        response = redirect(url_for("get_home"))
    elif user_role == db.UserRoleEnum.master:
        response = redirect(url_for("get_inscription"))
    else:
        abort(HTTPStatus.FORBIDDEN)

    session["user.id"] = user_id
    session["user.role"] = user_role
    #session["user.group"] = id_user_group
    session["user.group_name"] = name_user_group
    return response


@login_required
@app.route("/insert")
def init_db_command():
    conn = get_db_conn()
    id_user= session["user.id"]
    db.insertion_measure(id_user,conn)
    print(id_user)
    return redirect(url_for("get_home"))
    


@app.route("/logout", methods=["GET"])
@login_required
def get_logout():
    session.clear()
    return redirect(url_for("get_connexion"))


