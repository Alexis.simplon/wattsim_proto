# import base64
# from io import BytesIO
import plotly
import plotly.graph_objs as go
# from matplotlib.figure import Figure
import json
prod_objet_quotidien =[
{"nom_objet" : "Ampoules (éclairage)" , "consomation" : 200 },
{"nom_objet" : "Cuisinière électrique" , "consomation" : 550 },
#{"nom_objet" : "Réfrigérateur - congélateur (250 litres)" , "consomation" : 350 },
{"nom_objet" : "TV plasma" , "consomation" : 350 },
#{"nom_objet" : "TV LCD" , "consomation" : 225 },
{"nom_objet" : "Lave-vaisselle" , "consomation" : 225 },
#{"nom_objet" : "Four électrique" , "consomation" : 225 },
#{"nom_objet" : "Sèche-linge" , "consomation" : 200 },
#{"nom_objet" : "Fer à repasser" , "consomation" : 225 },
{"nom_objet" : "Machine à laver" , "consomation" : 200 },
{"nom_objet" : "Four à micro-ondes" , "consomation" : 110 },
{"nom_objet" : "Aspirateur" , "consomation" : 75 },
{"nom_objet" : "Sèche-cheveux" , "consomation" : 65 },
{"nom_objet" : "Ordinateur à écran plan" , "consomation" : 70 },
{"nom_objet" : "Cafetière" , "consomation" : 50 }
# {"nom_objet" : "Grille-pain" , "consomation" : 7.5}
]
def conversion_watt_heure(production:float)->float:
    production_en_heure = (production/(365*24))*1000
    return production_en_heure

def histograme_equivalence(valeur_prod, user):
    import matplotlib.pyplot as plt

    liste_objet = list()
    liste_valeur_objet_heure = list()
    for objet in prod_objet_quotidien:
        liste_objet.append(objet["nom_objet"])
        valeur =valeur_prod / objet["consomation"]
        liste_valeur_objet_heure.append(round(valeur)) 

    data = [
                go.Bar(
                    x= liste_objet,
                    y= liste_valeur_objet_heure
                )           
            ]
    graphJSON = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)
    # plt.subplots(figsize=(20,8)) 
    # plt.bar(range(10), liste_valeur_objet_heure, width = 0.4, color = 'red', edgecolor = 'blue', linewidth = 2, tick_label = liste_objet) 
    # plt.xticks(rotation = 90) 
    # plt.xlabel('Objet du quotidient',fontsize=15,color='darkblue') 
    # plt.ylabel('heure de charge produite par le simulateur', fontsize = 15, color = 'darkblue')
    # plt.title("heure de charge d'un objet grace a la production")
    # plt.savefig(f"static/img/{user}")
    # fig = Figure()
    # ax= fig.subplots()
    
    # ax.plot(range(10), liste_valeur_objet_heure, width = 0.4, color = 'red', edgecolor = 'blue', linewidth = 2, tick_label = liste_objet) 
    # # Save it to a temporary buffer.
    # buf = BytesIO()
    # fig.savefig(buf, format="png")
    #Embed the result in the html output.
    # data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return graphJSON

