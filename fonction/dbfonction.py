import psycopg2
from enum import Enum
from werkzeug.security import check_password_hash, generate_password_hash
from os import environ 
from .dbinit import select_user,Measurement, user_group, station, users, i_user, i_measurement, i_station, i_groupe_user,select_measure_classe, select_measure,select_last_measure_classe, select_last_measure
from random import randrange
from datetime import datetime


class UserRoleEnum(str, Enum):
    student = "student"
    master = 'master'


user_roles = [r.value for r in UserRoleEnum]
################ Connexion ################
def connection():
    try:
        conn = psycopg2.connect(
            dbname= environ.get('DB_NAME'), 
            user= environ.get('DB_USER'), 
            password= environ.get('DB_PASSWORD'), 
            host= environ.get('DB_HOST'),
            port= environ.get('DB_PORT')
            )
        print(conn)
    except:
        conn= None

        print("Connexion impossible. Tu as bien lancé docker compose ?")
    return conn


def DeconnexionBD(conn):
    try:
        conn.close()
    except:
        print('la deconnexion à échoué')


def select_donnee(CodeSQL:str, MonTuple:list ,conn)->list:
    cur= conn.cursor()
    try:
        cur.execute(CodeSQL, MonTuple)
        conn.commit()
    except psycopg2.Error as e:
        print(f'la requete : {CodeSQL} à échoué avec les valeurs : {MonTuple}')
        print(e)
    try :
        id_table = cur.fetchone()
    except :
        id_table=None
    
    return id_table
    


def supprimer_table(nom_table : str) -> None:
    conn,cur =Connexion()
    nom_table=str(nom_table)
    q = """ DROP TABLE %s """
    with conn:
        try:
            cur.execute(q,nom_table)
            print(f'la table {nom_table} à bien était détruite')
        except Error:
            print(Error)
    DeconnexionBD(conn)


    #########################   UTILISATEUR   #########################""


def get_utilisateur(conn, user_id):
    user = None

    query = """
        SELECT * FROM users
        WHERE id = %s
    """

    try:
        with conn:
            cur=conn.cursor()
            cur.execute(query, (user_id,))
            user = cur.fetchone()
    except psycopg2.Error:
        raise ValueError

    return user


def authentification_utilisateur(conn, username, password):
    if not username or not password:
        raise ValueError

    user = None

    query = """
        SELECT * FROM users
        WHERE username = %s
    """
    s_user_group = """
        
        SELECT nom_groupe FROM group_user
        WHERE id_user = %s
    """
    try:
        with conn:
            cur=conn.cursor()
            cur.execute(query, (username,))
            user = cur.fetchone()
            cur.execute(s_user_group,(user[0],))
            nom_groupe= cur.fetchone()
            
    except psycopg2.Error:
        raise ValueError
    print(user)
    print(user[2], " password hash ")
    if user and check_password_hash(user[2], password):
        return (user, nom_groupe)

    raise ValueError

def create_db_schema(conn):
    if conn == None:
        print('probleme de conn')
    with conn:
        cur=conn.cursor()
        cur.execute(
            users
        )
        #cur.execute(groupe)
        cur.execute(user_group)
        cur.execute(Measurement)
        cur.execute(station)


def insertion_measure(id_user, conn):
    hydro=randrange(1900, 3550)
    solar=randrange(0, 4500)
    wind=randrange(0, 4500)
    
    recorded_Date = datetime.now()
    print(solar)
    insert_measure(id_user, solar, wind, hydro, recorded_Date, conn)


def drop_db_schema(conn):
    with conn:
        cur=conn.cursor()
        cur.execute("""DROP TABLE if exists station""")
        cur.execute("""DROP TABLE if exists measurement""")
        cur.execute("""DROP TABLE if exists group_user""") 
        cur.execute("""DROP TABLE if exists users""")
        cur.execute("""DROP TABLE if exists groupe""")


def create_user(conn, username, password, role, group):
    if not username or not password or role not in user_roles:
        raise KeyError

    password_hash = generate_password_hash(password)
    values = (username,password_hash,role)
    print(conn)
    try:
        with conn:
            cur=conn.cursor()
            cur.execute(i_user,values)
            id_user = cur.fetchone()
            # cur.execute(i_groupe,(group,))
            # id_groupe = cur.fetchone()
            cur.execute(i_groupe_user,(group,id_user))

    except psycopg2.Error:
        raise ValueError

def insert_measure(id_user : int, solar : float, wind : float, hydro : float, recorded_Date, conn):
    id_measure = select_donnee(i_measurement,(solar,wind,hydro,recorded_Date),conn)
    # conn= connection()
    # cur=conn.cursor()
    # cur.execute()
    # id_measure= cur.fetchone()
    #cur.execute(i_station,(id_user,id_measure))
    print(id_measure)
    _ = select_donnee(i_station,(id_user,id_measure[0]),conn)

def select_liste_measure(conn,role : str, id_user : int, nom_groupe:str)-> list:
    cur = conn.cursor()

    if role == 'master':
        cur.execute(select_measure_classe, (nom_groupe,))
    elif role == 'student':
        cur.execute(select_measure, (id_user,))
    
    liste_measure = cur.fetchall()

    return liste_measure

def moyenne_measure(liste_measure: list):
    hydro = 0.0
    wind = 0.0
    solar = 0.0
    nb_measure = len(liste_measure)

    for measure in liste_measure:
        solar += measure[1] 
        wind += measure[2] 
        hydro += measure[3] 

    solar = solar/ nb_measure
    hydro = hydro / nb_measure
    wind = wind / nb_measure

    return (solar,wind,hydro)

def select_last_liste_measure(conn,role : str, id_user : int, nom_groupe:str)-> list:
    cur = conn.cursor()

    if role == 'master':
        cur.execute(select_last_measure_classe, (nom_groupe,))
        liste_measure = cur.fetchall()
    elif role == 'student':
        cur.execute(select_last_measure, (id_user,))
        liste_measure = cur.fetchone()
    

    return liste_measure

def selection_user(conn,id_user):
    cur = conn.cursor()
    cur.execute(select_user, (id_user,))
    user = cur.fetchone()
    

    return user


if __name__ == "__main__":
    pass
