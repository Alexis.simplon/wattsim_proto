station = """ create table if not exists station (
    id_user integer ,
    id_measure integer,
    PRIMARY KEY(id_user,id_measure),
    FOREIGN KEY(id_user) REFERENCES users(id),
    FOREIGN KEY(id_measure) REFERENCES measurement(id_measure)
    )
    """


i_station = """ insert into station (id_user, id_measure) 
  VALUES (%s, %s);
"""

Measurement = """ Create table if not exists measurement (
    id_measure SERIAL PRIMARY KEY,
    solar FLOAT ,
    wind FLOAT ,
    hydro FLOAT ,
    recorded_Date TIMESTAMP NOT NULL
    
  )"""
  
i_measurement = """ insert into measurement (solar, wind, hydro, recorded_Date) 
  VALUES (%s, %s, %s, %s) returning id_measure;
"""


users ="""
            CREATE TABLE if not exists users (
                id SERIAL PRIMARY KEY,
                username VARCHAR(255) UNIQUE NOT NULL,
                password_hash VARCHAR(255) NOT NULL,
                role TEXT
            )
        """

i_user = """
        INSERT INTO users (username, password_hash, role)
        VALUES (%s,%s,%s) returning id;
    """



# groupe = """ create table if not exists groupe(
#     id_groupe SERIAL PRIMARY KEY,
#     nom_groupe TEXT not null
# )
# """

# i_groupe = """
#         INSERT INTO groupe (nom_groupe)
#         VALUES (%s) returning id_groupe;
#     """
user_group = """ create table if not exists group_user (

    id_user INTEGER not null,
    nom_groupe TEXT not null,
    PRIMARY KEY(nom_groupe,id_user),
    FOREIGN KEY(id_user) REFERENCES users(id)

)
"""
i_groupe_user = """

    INSERT INTO group_user (nom_groupe,id_user)
    VALUES (%s,%s) ;

    """

select_measure = """SELECT * FROM measurement WHERE id_measure IN (

    SELECT id_measure FROM station WHERE id_user = %s

)

"""

select_measure_classe = """SELECT * FROM measurement WHERE id_measure IN 
(
        SELECT id_measure FROM station WHERE id_user IN 
    (
        SELECT id_user FROM group_user WHERE nom_groupe = %s
    )
)

"""

select_last_measure = """SELECT * FROM measurement WHERE id_measure IN (

    SELECT id_measure FROM station WHERE id_user = %s

) ORDER BY recorded_date DESC LIMIT 1
"""

select_last_measure_classe = """
        SELECT id , username, role FROM users WHERE id IN 
    (
        SELECT id_user FROM group_user WHERE nom_groupe = %s
    ) AND role = 'student'
 """#

select_user = """
SELECT id , username, role FROM users WHERE id = %s

"""